const MANUAL_TIME_OFFSET = 120;

let weather_chart = null;
let chart_options = {
	chart: {
		renderTo: 'weather-chart',
		zoomType: 'x'
	},
	title: {
		floating: true,
		text: ''
	},
	tooltip: {
		xDateFormat: '%d.%m.%Y %H:%M',
		shared: true,
		split: true
	},
	legend: {
		enabled: false
	},
	plotOptions: {
		area: {
			marker: {
				radius: 2
			},
			lineWidth: 2,
			states: {
				hover: {
					lineWidth: 3
				}
			},
			threshold: null
		}
	},
	xAxis: {
		type: 'datetime',
		labels: {
			format: '{value:%d.%m.%Y %H:%M Uhr}'
		},
		plotLines: []
	},
	yAxis: [{
		labels: {
			enabled: false
		},
		title: {
			enabled: false
		}
	}, {
		labels: {
			enabled: false
		},
		title: {
			enabled: false
		}
	}, {
		labels: {
			enabled: false
		},
		title: {
			enabled: false
		},
		opposite: true
	}],
	series: [{
		name: 'Luftdruck',
		code: 'pressure',
		type: 'area',
		data: [],
		yAxis: 2,
		legendIndex: 2,
		color: Highcharts.getOptions().colors[9],
		fillColor: {
			linearGradient: {
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1.1
			},
			stops: [
				[0, Highcharts.getOptions().colors[9]],
				[1, Highcharts.Color(Highcharts.getOptions().colors[9]).setOpacity(0).get('rgba')]
			]
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}mbar</b>'
		}
	}, {
		name: 'Luftfeuchtigkeit',
		code: 'humidity',
		type: 'area',
		data: [],
		yAxis: 1,
		legendIndex: 1,
		color: Highcharts.getOptions().colors[0],
		fillColor: {
			linearGradient: {
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1.1
			},
			stops: [
				[0, Highcharts.getOptions().colors[0]],
				[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
			]
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}%</b>'
		}
	}, {
		name: 'Temperatur',
		code: 'temperature',
		type: 'area',
		data: [],
		yAxis: 0,
		legendIndex: 0,
		color: Highcharts.getOptions().colors[5],
		fillColor: {
			linearGradient: {
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1.1
			},
			stops: [
				[0, Highcharts.getOptions().colors[5]],
				[1, Highcharts.Color(Highcharts.getOptions().colors[5]).setOpacity(0).get('rgba')]
			]
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}°C</b>'
		}
	}]
};

document.addEventListener('DOMContentLoaded', function () {

	// init weather chart (empty)
	weather_chart = Highcharts.chart(chart_options);

	// initially load data into the chart
	$.when(load_weather_data(null, null, [$('[name="initial-location"]').val()])).done( (data) => {
		updateWeatherChart(data, [$('[name="initial-metric"]').val()]);
	});

	// date-range picker
	$('.time-range-actions [data-timerange="custom"]').daterangepicker({
		opens: 'left',
		drops: 'up'
	}, function(start, end, label) {
		$('.time-range-actions [data-timerange="custom"] span').html(start.format('DD.MM.YYYY') + ' bis ' + end.format('DD.MM.YYYY'));
		$.when(load_weather_data(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'), [$('[name="initial-location"]').val()])).done( (data) => {
			updateWeatherChart(data, currently_active_series());
		});
	});

	// listener for time-range-actions
	$('.time-range-actions a').on('click', function(e) {
		$('.time-range-actions a').removeClass('active');
		$(this).addClass('active');

		let time_range_preset = $(this).data('timerange');
		if (time_range_preset !== 'custom') {
			$('.time-range-actions [data-timerange="custom"] span').html('&nbsp;Benutzerdefiniert');
			let timerange = getTimerangeDates(time_range_preset);
			$.when(load_weather_data(timerange.start, timerange.end, [$('[name="initial-location"]').val()])).done( (data) => {
				updateWeatherChart(data, currently_active_series());
			});
		}
	});

	// listener for chart-type toggle
	$('.chart-type').on('click', function(e) {
		$('.chart-type').toggleClass('hidden');
		let new_chart_type = $(this).data('charttype');

		let pressure_index = weather_chart.series.findIndex(elem => elem.name === 'Luftdruck');
		changeSeriesType(weather_chart.series[pressure_index], new_chart_type);
		let humidity_index = weather_chart.series.findIndex(elem => elem.name === 'Luftfeuchtigkeit');
		changeSeriesType(weather_chart.series[humidity_index], new_chart_type);
		let temperature_index = weather_chart.series.findIndex(elem => elem.name === 'Temperatur');
		changeSeriesType(weather_chart.series[temperature_index], new_chart_type);
	});

	// listener for custom legend
	$('.custom-legend .btn').on('click', function(e) {
		let metric = $(this).data('metric');
		let series_index = weather_chart.series.findIndex(elem => elem.name === metric);
		let series = weather_chart.series[series_index];
		if (series.visible) {
			series.hide();
			$(this).addClass('disabled');
		} else {
			series.show();
			$(this).removeClass('disabled');
		}
	});

	/**
	 * Updates the weather chart with the passed data.
	 * @param data Array containing data for 'temperature', 'humidity', 'pressure'.
	 * @param show_series Array containing names of chart-series to display (defaults to 'temperature').
	 * @returns {{yAxis: *[], xAxis: {type: string, labels: {format: string}}, series: *[], chart: {zoomType: string, type: string}}}
	 */
	function updateWeatherChart(data, show_series = ['Temperatur']) {
		// empty chart series that no data is provided for
		if (!data.temperature) {
			weather_chart.series[seriesIndex('Temperatur')].setData([], false);
		}
		if (!data.humidity) {
			weather_chart.series[seriesIndex('Luftfeuchtigkeit')].setData([], false);
		}
		if (!data.pressure) {
			weather_chart.series[seriesIndex('Luftdruck')].setData([], false);
		}
		if (!data.temperature || !data.temperature || !data.temperature) {
			weather_chart.redraw();
		}

		let chart_data = {
			temperature: [],
			humidity: [],
			pressure: []
		};

		Object.keys(data).forEach( (key) => {
			let dataset = data[key];
			for (let data_point_index = 0; data_point_index < dataset.length; data_point_index++) {
				// convert date-string to timestamp
				let split = dataset[data_point_index][0].split(/[^0-9]/);
				let timestamp = new Date(...split).getTime();

				//let date = new Date(dataset[data_point_index][0]);
				//let timestamp = date.getTime() + date.getTimezoneOffset() + MANUAL_TIME_OFFSET * 60000;
				let formatted_point = [timestamp, dataset[data_point_index][1]];
				chart_data[key].push(formatted_point);

				// add plot lines for separating weeks
				if (key === 'temperature' && startOfWeek(dataset[data_point_index][0])) {
					upgradePlotLine(weather_chart.xAxis[0], {
						color: 'lightgray',
						value: timestamp,
						label: {
							text: 'KW ' + moment(dataset[data_point_index][0]).week(),
							align: 'left'
						}
					});
				}
			}
		});

		if (chart_data.temperature) {
			weather_chart.series[seriesIndex('Temperatur')].setData(chart_data.temperature, false);
		}
		if (chart_data.humidity) {
			weather_chart.series[seriesIndex('Luftfeuchtigkeit')].setData(chart_data.humidity, false);
		}
		if (chart_data.pressure) {
			weather_chart.series[seriesIndex('Luftdruck')].setData(chart_data.pressure, false);
		}

		weather_chart.series.forEach( (series) => {
			let show = (show_series.indexOf(series.name) >= 0);
			series.visible = show;
			if (!show) {
				$('.custom-legend [data-metric="' + series.name + '"]').addClass('disabled');
			}
		});

		weather_chart.redraw();
		update_series_visibility();
		hide_loader();
	} // end updateWeatherChart()

}); // end DOMContentLoaded()

/**
 * Loads data for the weather chart in a specified time range. (Defaults to today)
 * @param date_start 	Start date of the time range.
 * @param date_end		End date of the time range.
 * @param locations		Array of locations (inside and/or outside).
 * @returns {*|jQuery|{getAllResponseHeaders, abort, setRequestHeader, readyState, getResponseHeader, overrideMimeType, statusCode}}
 */
function load_weather_data(date_start = null, date_end = null, locations = ['outside']) {
	show_loader();

	if (!date_start || !date_end) {
		let dates = getTimerangeDates();
		date_start = dates.start;
		date_end = dates.end;
	}

	return $.ajax({
		url: './dashboard/getStatistics',
		type: 'POST',
		data: {
			user_code: $('[name="user"]').val(),
			location: locations[0],
			date_start: date_start,
			date_end: date_end
		}
	});
} // end load_weather_data()

let prev_date = null;

/**
 * Checks if the passed date is the first (known) date of a week.
 * @param date Datetime string
 * @returns {boolean} True if first, otherwise false.
 */
function startOfWeek(date) {
	let is_monday = (moment(date).day() === 1);
	let is_first_hour = (moment(date).hour() === 0);
	let if_first_in_hour = (moment(date).format('YYYY-MM-DD H') !== prev_date);

	prev_date = moment(date).format('YYYY-MM-DD H');

	return is_monday && is_first_hour && if_first_in_hour;
} // end startOfWeek()

/**
 * Returns the start and end date for a time-range code. Defaults to last day.
 * @param preset_code The code for the timerange.
 * @returns {{start: null, end: *}} Start and end date of the timerange.
 */
function getTimerangeDates(preset_code) {
	let timerange = {
		start: null,
		end: moment().format('YYYY-MM-DD')
	};

	switch (preset_code) {
		case 'today':
			timerange.start = moment().format('YYYY-MM-DD');
			break;
		case 'week':
			timerange.start = moment().subtract(1, 'weeks').format('YYYY-MM-DD');
			break;
		case 'month':
			timerange.start = moment().subtract(1, 'months').format('YYYY-MM-DD');
			break;
		case 'year':
			timerange.start = moment().subtract(1, 'years').format('YYYY-MM-DD');
			break;
		default:
			timerange.start = moment().format('YYYY-MM-DD');
	}

	return timerange;
} // end getTimerangeDates()

/**
 * Updates the type of a series to the new type.
 * @param series Highcharts chart series.
 * @param new_type New chart type to set.
 */
function changeSeriesType(series, new_type) {
	let data_points = series.data;

	let raw_data_points = [];
	for (let i = 0; i < data_points.length; i++) {
		if (typeof data_points[i] !== undefined) {
			raw_data_points[i] = [data_points[i].x, data_points[i].y];
		}
	}

	weather_chart.addSeries({
		name: series.options.name,
		code: series.options.code,
		type: new_type,
		data: raw_data_points,
		yAxis: series.options.yAxis,
		legendIndex: series.options.legendIndex,
		color: series.options.color,
		fillColor: series.options.fillColor,
		visible: series.options.visible
	}, false);

	series.remove();
} // end changeSeriesType()

/**
 * Returns the index of the series with the specified name.
 * @param series_name Name of the series.
 * @returns {number} Index of the series in the charts series array.
 */
function seriesIndex(series_name) {
	return weather_chart.series.findIndex(elem => elem.name === series_name);
} // end seriesIndex()

/**
 * Add new plotline.
 * @param axis Axis object to add the plotline to.
 * @param new_line The plotline to add to the axis.
 */
function upgradePlotLine(axis, new_line) {
	axis.removePlotLine('' + new_line.label.value);
	axis.addPlotLine(new_line);
} // end upgradePlotLine()

function currently_active_series() {
	let active_series = [];
	weather_chart.series.forEach( (series) => {
		if (series.visible) {
			active_series.push(series.name);
		}
	});
	return active_series;
} // end currently_active_series()

function update_series_visibility() {
	weather_chart.series.forEach( (series) => {
		if (series.visible) {
			series.show();
			$('.custom-legend [data-metric="' + series.name + '"]').removeClass('disabled');
		} else {
			series.hide();
			$('.custom-legend [data-metric="' + series.name + '"]').addClass('disabled');
		}
	});
} // end update_series_visibility()

function show_loader() {
	$('.loader').removeClass('hidden');
} // end show_loader()

function hide_loader() {
	$('.loader').addClass('hidden');
} // end hide_loader()
