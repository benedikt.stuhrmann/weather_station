<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Maps a user-code to a username (E.g.: 'hs' => 'Hendrik').
 * @param string $user_code The users code (E.g.: 'hs', 'bs', 'rs').
 * @return string The users name.
 *                If no username was found to match the specified code, a redirect to the home page will be done.
 */
function mapUserCodeToName(string $user_code): string {
  $CI = get_instance();
  $weather_station_users = $CI->config->item('ws_users');
  foreach ($weather_station_users as $user) {
    if ($user->user_code === $user_code) {
      return $user->user_name;
    }
  }
  // redirect to home when dashboard is unknown
  redirect(base_url());
} // end mapUserCodeToName()

/**
 * Returns the sensor names for a specific user.
 * @param string $user_code Code of the user.
 * @param string|null $location Location of the sensor. Either WeatherEventObj::LOCATION_INSIDE or WeatherEventObj::LOCATION_OUTSIDE.
 *                              Null for all sensors of the user, ignoring their location.
 * @return String[] Array of sensor names.
 */
function getSensors(string $user_code, string $location = null): array {
  $CI = get_instance();
  $weather_station_users = $CI->config->item('ws_users');

  // search the user by code
  foreach ($weather_station_users as $user) {
    if ($user->user_code === $user_code) {
      // get the sensors for the specified location
      $sensors = [];
      foreach ($user->sensors as $sensor_name => $sensor_location) {
        if ($location === null  || $location === $sensor_location) {
          $sensors[] = $sensor_name;
        }
      }
      return $sensors;
    }
  }

  return [];
} // end getSensors()
