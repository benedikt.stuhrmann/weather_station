<?php
defined('BASEPATH') || exit('No direct script access allowed');

use DataObjects\WeatherEventObj;

class Weather_event_model extends CI_Model {

  /**
   * Weather_event_model constructor.
   * Connects to the database.
   */
  public function __construct() {
    parent::__construct();

    $this->load->database(WEATHER_DB);
  } // end __construct();

/**
   * Gets the most recent weather-event of the specified sensor.
   * @param string $sensor   Has to be a known sensor name.
   * @return WeatherEventObj|null The weather-event.
   */
  public function getLatest(string $sensor) {
    if (WeatherEventObj::validSensor($sensor)) {
      $this->db->where('sensor', $sensor);
    }

    $weather_event = $this->db
      ->order_by('date', 'DESC')
      ->get('weather_event')
      ->first_row();

    if ($weather_event) {
			$weather_event = new WeatherEventObj([
				'id' => $weather_event->id,
				'sensor' => $weather_event->sensor,
				'location' => $weather_event->location,
				'date' => $weather_event->date,
				'temperature' => $weather_event->temperature,
				'humidity' => $weather_event->humidity,
				'pressure' => $weather_event->pressure
			]);
		}

    return $weather_event;
  } // end get()

  /**
	 * Returns all weather events of the specified sensors in the specified time range.
	 * @param DateTime $date_start 			Start date of the time range.
	 * @param DateTime $date_end				End date of the time range.
	 * @param array    $sensors					Array of sensor names to query for.
	 * @param bool     $force_stdClass	By default return type is an array of weather event objects. When setting this to false,
	 *                                  an array of simple objects will be returned. This results in a significant boost of performance
	 *                                  but does not convert properties to the correct data types.
	 * @return StdClass[]|WeatherEventObj[] The weather events as an array of weather event objects.
	 *                                  		(Or std classes when $force_stdClass was set to true)
	 */
  public function getFromTo(DateTime $date_start, DateTime $date_end, array $sensors, bool $force_stdClass = false) : array {
    $weather_events = $this->db
      ->where_in('sensor', $sensors)
      ->where('DATE(date) >= ', $date_start->format('Y-m-d'))
      ->where('DATE(date) <= ', $date_end->format('Y-m-d'))
      ->get('weather_event')
      ->result();

    if (!$force_stdClass) {
			$weather_events = WeatherEventObj::toWeatherEvents($weather_events);
		}

    return $weather_events;
  } // end getLatest()

	/**
   * Inserts the given weather-event into the database.
   * @param WeatherEventObj $weather_event
   * @return bool|WeatherEventObj The inserted weather-event. False, on failure.
   */
  public function insert(WeatherEventObj $weather_event) {
    $insert_data = [
      'date'        => ($weather_event->getDate()) ? $weather_event->getDate()->format('Y-m-d H:i:s') : null,
      'sensor'      => $weather_event->getSensor(),
      'location'    => $weather_event->getLocation(),
      'temperature' => $weather_event->getTemperature(),
      'humidity'    => $weather_event->getHumidity(),
      'pressure'    => $weather_event->getPressure()
    ];

    $success = $this->db->insert('weather_event', $insert_data);

    if ($success) {
      $insert_id = $this->db->insert_id();
      $weather_event = $this->get(['id' => $insert_id]);
      if (sizeof($weather_event)) {
        return $weather_event[0];
      }
    }

    return false;
  } // end getFromTo()

  /**
   * Gets an array of weather-events from the database that match the specified filter.
   * @param array $filter Array of filters.
   * @return WeatherEventObj[] Array of weather-events matching the filter.
   */
  public function get(array $filter = []): array {
    $weather_events = $this->db
      ->get_where('weather_event', $filter)
      ->result();

    $weather_events = WeatherEventObj::toWeatherEvents($weather_events);
    return $weather_events;
  } // end insert()

  /**
   * Returns the known sensor names/codes from the database.
   * @return String[] Currently known sensor names/codes.
   */
  public function getKnownSensors(): array {
    $column_type = $this->db->query('SHOW COLUMNS FROM weather_event WHERE Field = "sensor"')->row(0)->Type;
    preg_match("/^enum\(\'(.*)\'\)$/", $column_type, $matches);
    $sensor_names = explode("','", $matches[1]);
    return $sensor_names;
  } // end getKnownSensors()

} // end class
