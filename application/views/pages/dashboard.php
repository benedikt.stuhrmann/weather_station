<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/dashboard.css'>

<div class='weather-displays'>

	<?php if (!$latest_outside || !$latest_inside) {
		redirect(base_url());
	}?>

  <?php $this->load->view('layout/weather_display', [
    'location'        => $latest_outside->getLocation(),
    'location_label'  => 'Aussen',
    'temperature'     => $latest_outside->getTemperature(),
    'humidity'        => $latest_outside->getHumidity(),
    'pressure'        => $latest_outside->getPressure(),
    'datetime'        => $latest_outside->getDate()->format('H:i')
  ]) ?>

  <?php $this->load->view('layout/weather_display', [
    'location'        => $latest_inside->getLocation(),
    'location_label'  => 'Innen',
    'temperature'     => $latest_inside->getTemperature(),
    'humidity'        => $latest_inside->getHumidity(),
    'pressure'        => $latest_inside->getPressure(),
    'datetime'        => $latest_inside->getDate()->format('H:i')
  ]) ?>

</div>
