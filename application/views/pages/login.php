<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/login.css'>

<div id='login'>
	<div id='content'>
		<h1>Wetterstation</h1>
		<hr />
		<form method='post' action='./login/submit_login_form'>
			<input type='password' name='password' placeholder='Passwort'/>
		</form>
	</div>
</div>
