<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/daterangepicker.css' />
<link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/statistics.css'>

<script src='<?= base_url() ?>assets/js/moment.min.js' type='text/javascript'></script>
<script src='<?= base_url() ?>assets/js/daterangepicker.min.js' type='text/javascript' ></script>
<script src='<?= base_url() ?>assets/js/highcharts.src.js' type='text/javascript'></script>
<script src='<?= base_url() ?>assets/js/statistics.js' type='text/javascript'></script>

<div id='statistics'>

	<a href='<?= base_url() ?>dashboard/<?= $user_code ?>' class='btn btn-back'><i class='fas fa-fw fa-fh fa-arrow-left'></i></a>
	<div class='loader hidden'>
		<?php $this->load->view('layout/loader_growing_dots') ?>
	</div>
	<div class='btn-group chart-actions'>
		<a href='javascript:;' class='btn chart-type' data-charttype='line'><i class='fas fa-fw fa-fh fa-chart-line'></i></a>
		<a href='javascript:;' class='btn chart-type hidden' data-charttype='area'><i class='fas fa-fw fa-fh fa-chart-area'></i></a>
	</div>

  <div class='chart-container'>
		<input type='hidden' name='initial-metric' value='<?= $metric ?>'>
		<input type='hidden' name='initial-location' value='<?= $location ?>'>
		<input type='hidden' name='user' value='<?= $user_code ?>'>
    <div id='weather-chart' style='width: 100%; height: 400px;'></div>
		<div class='action-container'>
			<div class='btn-group time-range-actions'>
				<a href='javascript:;' class='btn active' data-timerange='today'>Heute</a>
				<a href='javascript:;' class='btn' data-timerange='week'>Woche</a>
				<a href='javascript:;' class='btn' data-timerange='month'>Monat</a>
				<a href='javascript:;' class='btn' data-timerange='year'>Jahr</a>
				<a href='javascript:;' class='btn' data-timerange='custom'><i class='far fa-fw fa-fh fa-calendar-alt'></i><span>&nbsp;Benutzerdefiniert</span></a>
			</div>
			<div class='btn-group custom-legend'>
				<a href='javascript:;' class='btn temperature' data-metric='Temperatur'>
					<span class='text'>Temperatur</span>
					<span class='symbol'><img class='icon' src='<?= base_url() ?>assets/icons/temperature.svg' alt='Symbol für Temperatur'/></span>
				</a>
				<a href='javascript:;' class='btn humidity' data-metric='Luftfeuchtigkeit'>
					<span class='text'>Luftfeuchtigkeit</span>
					<span class='symbol'><img class='icon' src='<?= base_url() ?>assets/icons/humidity.svg' alt='Symbol für Luftfeuchtigkeit'/></span>
				</a>
				<a href='javascript:;' class='btn pressure' data-metric='Luftdruck'>
					<span class='text'>Luftdruck</span>
					<span class='symbol'><img class='icon' src='<?= base_url() ?>assets/icons/pressure.svg' alt='Symbol für Luftdruck'/></span>
				</a>
			</div>
		</div>
  </div>

</div>
