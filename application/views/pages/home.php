<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/home.css'>

<div id='home'>
  <?php $this->load->view('layout/user_dashboard_box', [
    'user_name' => 'Hendrik',
    'user_code' => 'hs'
  ]) ?>
  <?php $this->load->view('layout/user_dashboard_box', [
    'user_name' => 'Ralf',
    'user_code' => 'rs'
  ]) ?>
  <?php $this->load->view('layout/user_dashboard_box', [
    'user_name' => 'Bene',
    'user_code' => 'bs'
  ]) ?>
</div>
