<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'>
    <title><?= $title ?></title>

		<link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/fontawesome.css'>
    <link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/main.css'>

    <script src='<?= base_url() ?>assets/js/jquery-3.4.1.min.js' type='text/javascript'></script>
  </head>
  <body>
    <?php $this->load->view('pages/' . $view) ?>
  </body>
</html>
