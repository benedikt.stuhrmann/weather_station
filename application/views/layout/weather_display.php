<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class='weather-display weather-<?= $location ?>'>
  <div class='main-content'>
    <h1 class='location'><?= $location_label ?></h1>
    <hr />
    <div class='current-stats'>
      <a class='temperature fly' href='<?= $user_code ?>/statistics?metric=temperature&location=<?= $location ?>' data-location='<?= $location ?>' data-metric='temperature'>
				<?= number_format($temperature, 2, ',', '') ?>°C
			</a>
      <div>
        <a class='humidity' href='<?= $user_code ?>/statistics?metric=humidity&location=<?= $location ?>' data-location='<?= $location ?>' data-metric='humidity'>
          <img class='icon-sm' src='<?= base_url() ?>assets/icons/humidity.svg' alt='Symbol für Luftfeuchtigkeit'/>
          <span><?= number_format($humidity, 2, ',', '') ?>%</span>
        </a>
        <a class='pressure' href='<?= $user_code ?>/statistics?metric=pressure&location=<?= $location ?>' data-location='<?= $location ?>' data-metric='pressure'>
          <img class='icon-sm' src='<?= base_url() ?>assets/icons/pressure.svg' alt='Symbol für Luftdruck'/>
          <span><?= number_format($pressure, 2, ',', '') ?>mbar</span>
        </a>
      </div>
    </div>
  </div>
  <div class='updated'>
    <i>Letzte Aktualisierung: <span class='last-updated'><?= $datetime ?></span> Uhr</i>
  </div>
</section>
