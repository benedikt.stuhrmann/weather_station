<?php

$config['ws_users'] = [
  (object) [
    'user_code' => 'hs',
    'user_name' => 'Hendrik',
    'sensors'   => [
      'hendrik_innen'  => 'inside',
      'hendrik_aussen' => 'outside'
    ]
  ],
  (object) [
    'user_code' => 'rs',
    'user_name' => 'Ralf',
    'sensors'   => [
      'ralf_innen'  => 'inside',
      'ralf_aussen' => 'outside'
    ]
  ],
  (object) [
    'user_code' => 'bs',
    'user_name' => 'Bene',
    'sensors'   => [
      'bene_innen'  => 'inside',
      'bene_aussen' => 'outside'
    ]
  ]
];
