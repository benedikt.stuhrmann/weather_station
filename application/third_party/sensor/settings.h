// Network settings
const char* ssid = "";
const char* password = "";
const char* espName = "";
const char* location = ""; // either "inside" or "outside"
const char* sensor_name = "";

// Minutes to sleep between updates
int minutes2sleep = 15;

// Use SSL
bool ssl_enabled = false;

// Device Token (unencrypted)
String auth_token = "";

// Endpoint settings
const char* url = ""; // URL for API call
const char* fingerprint = "79 B1 29 4A 6F CD DB 96 C8 96 03 36 AA 2F F7 D6 08 82 43 71"; //certificate fingerprint (for https usage)
const char* contenttype = "application/x-www-form-urlencoded";

String userAgent = ""; // Name of your weather station
String clientVer = "0.3";
