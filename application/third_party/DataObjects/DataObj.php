<?php
namespace DataObjects;

defined('BASEPATH') || exit('No direct script access allowed');

class DataObj {

  /**
   * @var CodeIgniter
   */
  protected $CI;

  /**
   * @var int Unique id.
   */
  private $id;

  /**
   * DataObj constructor.
   * @param array|null $attrs Array of data-obj attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    $this->CI = get_instance();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the data objects attributes with array values.
   * @param array $attrs Array of data-obj attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    if (array_key_exists('id', $attrs)) $this->setId($attrs['id']);
  } // end fromArray()

  /**
   * @return int|null
   */
  public function getId() {
    return $this->id;
  } // end getId

  /**
   * @param int $id
   */
  public function setId(int $id): void {
    $this->id = $id;
  } // end setId

} // end class
