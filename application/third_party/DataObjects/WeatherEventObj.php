<?php
namespace DataObjects;

use DateTime;
use Exception;

defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Class WeatherEventObj
 * @package DataObjects
 * @property-read \Weather_event_model weather_event_model
 */
class WeatherEventObj extends DataObj {

  /**
   * @var string Constant for location inside.
   */
  const LOCATION_INSIDE = 'inside';

  /**
   * @var string Constant for location outside
   */
  const LOCATION_OUTSIDE = 'outside';

  /**
   * @var DateTime Time when the weather event got created.
   */
  private $date;

  /**
   * @var string The name/code of the sensor that measured the event.
   */
  private $sensor;

  /**
   * @var string The location the event refers to. One of LOCATION_INSIDE and LOCATION_OUTSIDE.
   */
  private $location;

  /**
   * @var float Temperature in degrees celsius.
   */
  private $temperature;

  /**
   * @var float Humidity in percent.
   */
  private $humidity;

  /**
   * @var float Air-pressure in millibar.
   */
  private $pressure;

  /**
   * WeatherEventObj constructor.
   * @param array|null $attrs Array of weather-event attributes where the keys are the attribute-names.
   */
  public function __construct(array $attrs = null) {
    parent::__construct();

    if ($attrs) $this->fromArray($attrs);
  } // end __construct()

  /**
   * Sets the weather-event objects attributes with array values.
   * @param array $attrs Array of weather-event attributes where the keys are the attribute-names.
   */
  public function fromArray(array $attrs) {
    parent::fromArray($attrs);

    if (array_key_exists('date', $attrs))         $this->setDate($attrs['date']);
    if (array_key_exists('sensor', $attrs))       $this->setSensor($attrs['sensor']);
    if (array_key_exists('location', $attrs))     $this->setLocation($attrs['location']);
    if (array_key_exists('temperature', $attrs))  $this->setTemperature($attrs['temperature']);
    if (array_key_exists('humidity', $attrs))     $this->setHumidity($attrs['humidity']);
    if (array_key_exists('pressure', $attrs))     $this->setPressure($attrs['pressure']);
  } // end fromArray()

  /**
   * Checks if the specified location is a valid location.
   * @param string $location Location to check.
   * @return bool True, if valid. False otherwise.
   */
  public static function validLocation(string $location): bool {
    return ($location === self::LOCATION_INSIDE || $location === self::LOCATION_OUTSIDE);
  } // end validLocation()

  /**
   * Checks if the specified sensor is a known one.
   * @param string $sensor Sensor name/code to check.
   * @return bool True, if valid. False otherwise.
   */
  public static function validSensor(string $sensor): bool {
    $CI = get_instance();
    $known_sensors = $CI->weather_event_model->getKnownSensors();
    return in_array($sensor, $known_sensors);
  } // end validSensor()

  /**
   * Transforms an array with weather event data (e.g. from the database) to an array of WeatherEventObjs.
   * @param array $weather_event_data Array with weather event data.
   * @return WeatherEventObj[] An array of weather events.
   */
  public static function toWeatherEvents(array $weather_event_data) : array {
    foreach ($weather_event_data as &$weather_event) {
			$weather_event = new WeatherEventObj([
        'id'          => $weather_event->id,
        'sensor'      => $weather_event->sensor,
        'location'    => $weather_event->location,
        'date'        => $weather_event->date,
        'temperature' => $weather_event->temperature,
        'humidity'    => $weather_event->humidity,
        'pressure'    => $weather_event->pressure
      ]);
    }

    return $weather_event_data;
  } // end toWeatherEvents()

  /**
   * @return DateTime|null
   */
  public function getDate() {
    return $this->date;
  } // end getDate()

  /**
   * @param DateTime|string $date
   */
  public function setDate($date): void {
    if (is_string($date)) {
      try {
        $this->date = new DateTime($date);
      } catch(Exception $e) {
        echo $e->getMessage();
      }
    } else if ($date instanceof DateTime) {
      $this->date = $date;
    }
  } // end setDate()

  /**
   * @return string|null
   */
  public function getSensor() {
    return $this->sensor;
  } // end getSensor()

  /**
   * Has to be a known sensor name.
   * @param string $sensor
   */
  public function setSensor(string $sensor): void {
    if (!self::validSensor($sensor)) {
      return;
    }
    $this->sensor = $sensor;
  } // end setSensor()

  /**
   * @return string|null
   */
  public function getLocation() {
    return $this->location;
  } // end getLocation()

  /**
   * Has to be either WeatherEventObject::LOCATION_INSIDE or WeatherEventObject::LOCATION_OUTSIDE.
   * @param string $location
   */
  public function setLocation(string $location): void {
    if (!self::validLocation($location)) {
      return;
    }
    $this->location = $location;
  } // end setLocation()

  /**
   * @return float|null
   */
  public function getTemperature() {
    return $this->temperature;
  } // end getTemperature()

  /**
   * @param float $temperature
   */
  public function setTemperature(float $temperature): void {
    $this->temperature = $temperature;
  } // end setTemperature()

  /**
   * @return float|null
   */
  public function getHumidity() {
    return $this->humidity;
  } // end getHumidity()

  /**
   * @param float $humidity
   */
  public function setHumidity(float $humidity): void {
    $this->humidity = $humidity;
  } // end setHumidity()

  /**
   * @return float|null
   */
  public function getPressure() {
    return $this->pressure;
  } // end getPressure()

  /**
   * @param float $pressure
   */
  public function setPressure(float $pressure): void {
    $this->pressure = $pressure;
  } // end setPressure()

} // end class
