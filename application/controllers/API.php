<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use DataObjects\WeatherEventObj;

class API extends CI_Controller {

  public function __construct() {
    parent::__construct();

    $this->load->model('weather_event_model');
		$this->load->config('auth_tokens');
  } // end __construct()

  /**
   * Adds a new data point measured by the sensor.
   * Request has to contain 'sensor', 'location', 'temperature', 'humidity' and 'pressure'.
   */
  public function add() {
    $sensor = $this->input->post('sensor');
    $location = $this->input->post('location');
    $temperature = $this->input->post('temperature');
    $humidity = $this->input->post('humidity');
    $pressure = $this->input->post('pressure');
    $auth_token = $this->input->post('auth_token');

    // check request method
    if ($this->input->method(TRUE) !== 'POST') {
      $response = [
        'response_code' => 405,
        'message' => $this->input->method(TRUE) . ' request is not allowed. Try POST instead.'
      ];

      return $this->output
        ->set_header('HTTP/1.1 405 Method Not Allowed')
        ->set_content_type('Content-Type: application/json')
        ->set_output(json_encode($response));
    }

    // check sensor
    if (!WeatherEventObj::validSensor($sensor)) {
      $response = [
        'response_code' => 400,
        'message' => 'Unknown sensor.'
      ];

      return $this->output
        ->set_header('HTTP/1.1 400 400 Bad request')
        ->set_content_type('Content-Type: application/json')
        ->set_output(json_encode($response));
    }


    // check authentication
    if (!$auth_token || !$this->authTokenValid($auth_token)) {
      $response = [
        'response_code' => 401,
        'message' => 'No auth-token provided or invalid.'
      ];

      return $this->output
        ->set_header('HTTP/1.1 401 Unauthorized ')
        ->set_content_type('Content-Type: application/json')
        ->set_output(json_encode($response));
    }

    // check if a valid location was specified
    if (!WeatherEventObj::validLocation($location)) {
      $response = [
        'response_code' => 400,
        'message' => 'No weather-event location provided.'
      ];

      return $this->output
        ->set_header('HTTP/1.1 400 Bad request')
        ->set_content_type('Content-Type: application/json')
        ->set_output(json_encode($response));
    }

    // create weather event
    $weather_event = new WeatherEventObj();
    $weather_event->setDate(new DateTime());
    $weather_event->setLocation($location);
		$weather_event->setSensor($sensor);

    if (is_numeric($temperature)) {
      $weather_event->setTemperature(floatval($temperature));
    }
    if (is_numeric($humidity)) {
      $weather_event->setHumidity(floatval($humidity));
    }
    if (is_numeric($pressure)) {
      $weather_event->setPressure(floatval($pressure));
    }

    if (!empty($weather_event->getTemperature()) || !empty($weather_event->getHumidity()) || !empty($weather_event->getPressure())) {
      // save it (if there's at least one value)
      $weather_event = $this->weather_event_model->insert($weather_event);
      if ($weather_event === false) {
        $response = [
          'response_code' => 500,
          'message' => 'An error occurred while trying to save the weather-event. Please try again later.'
        ];

        return $this->output
          ->set_header('HTTP/1.1 201 Created')
          ->set_content_type('Content-Type: application/json')
          ->set_output(json_encode($response));
      }

      $response = [
        'response_code' => 201,
        'message' => 'A new weather-event got added successfully: Date: ' . $weather_event->getDate()->format('Y-m-d H:i:s') . ', Location: ' . $weather_event->getLocation() . ', Temperature: ' . $weather_event->getTemperature() . ', Humidity: ' . $weather_event->getHumidity() . ', Pressure: ' . $weather_event->getPressure()
      ];

      return $this->output
        ->set_header('HTTP/1.1 201 Created')
        ->set_content_type('Content-Type: application/json')
        ->set_output(json_encode($response));
    } else {
      // no values submitted => throw error
      $response = [
        'response_code' => 400,
        'message' => 'No weather-event data provided.'
      ];

      return $this->output
        ->set_header('HTTP/1.1 400 Bad request')
        ->set_content_type('Content-Type: application/json')
        ->set_output(json_encode($response));
    }
  } // end add()

  /**
   * Gets the latest metric-value for the specified sensor.
   * @param string|null $sensor The sensor to get data from.
   * @param string|null $metric The metric. One of: 'temperature', 'humidity', 'pressure'
   * @param string|null $auth_token A valid authentication-token.
   * @return mixed
   */
  public function get(string $sensor = null, string $metric = null, string $auth_token = null) {
    // check authentication
    if (!$auth_token || !$this->authTokenValid($auth_token)) {
      return $this->output
        ->set_header('HTTP/1.1 401 Unauthorized ')
        ->set_content_type('Content-Type: application/json')
        ->set_output(json_encode([
          'response_code' => 401,
          'message' => 'No auth-token provided or invalid.'
        ]));
    }

    // check sensor
    if (!WeatherEventObj::validSensor($sensor)) {
      return $this->output
        ->set_header('HTTP/1.1 400 Bad request')
        ->set_content_type('Content-Type: application/json')
        ->set_output(json_encode([
          'response_code' => 400,
          'message => Unknown sensor.'
        ]));
    }

    $weather_event = $this->weather_event_model->getLatest($sensor);

    $value = null;
    switch ($metric) {
      case 'temperature':
        $value = $weather_event->getTemperature();
        break;
      case 'humidity':
        $value = $weather_event->getHumidity();
        break;
      case 'pressure':
        $value = $weather_event->getPressure();
        break;
      default:
        // unknown metric
        return $this->output
          ->set_header('HTTP/1.1 400 Bad request')
          ->set_content_type('Content-Type: application/json')
          ->set_output(json_encode([
            'response_code' => 400,
            'message => Invalid metric specified.'
          ]));
    }

    return $this->output
      ->set_header('HTTP/1.1 200 OK')
      ->set_content_type('Content-Type: application/json')
      ->set_output(json_encode($value));
  } // end get()

  /**
   * Checks if the provided auth-token is valid.
   * @param string $auth_token The auth-token passed with the request.
   * @return bool True, if valid. False otherwise.
   */
  private function authTokenValid(string $auth_token): bool {
  	$auth_tokens = $this->config->item('auth_tokens');
  	$valid = in_array($auth_token, $auth_tokens);
    return $valid;
  } // end authTokenValid

} // end class
