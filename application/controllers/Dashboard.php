<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Class Dashboard
 * @property-read Weather_event_model weather_event_model
 */
class Dashboard extends CI_Controller {

  public function __construct() {
    parent::__construct();

		session_start();
		$logged_in = (array_key_exists('login', $_SESSION) ? $_SESSION['login'] : false) === true;
		if (!$logged_in) {
			redirect('login');
		}

    $this->load->model('weather_event_model');
  } // end __construct()

  /**
   * Remaps URL calls to controller functions to bypass Codeigniters controller/method/params URL structure for better readability.
   * @param $user_code The users code (E.g.: 'hs', 'bs', 'rs').
   * @param $params Array of URL query parameters.
   */
  function _remap($user_code, $params) {
    if (in_array('getStatistics', $params)) {
      $this->getStatistics();
      return;
    }
    if (in_array('statistics', $params)) {
      $this->statistics($user_code);
    } else {
      $this->dashboard($user_code);
    }
  } // end _remap()

  /**
   * Shows the users dashboard with latest inside and outside data.
   * @param string $user_code The users code (E.g.: 'hs', 'bs', 'rs').
   */
  public function dashboard(string $user_code) {
    $user_name = mapUserCodeToName($user_code);

    $this->load->view('frame', [
      'view'            => 'dashboard',
      'title'           => 'Wetterstation - ' . $user_name,
      'user_code'       => $user_code,
      'user_name'       => $user_name,
      'latest_outside'  => $this->weather_event_model->getLatest(lcfirst($user_name) . '_aussen'),
      'latest_inside'   => $this->weather_event_model->getLatest(lcfirst($user_name) . '_innen')
    ]);
  } // end dashboard()

  /**
   * Shows the users statistics page with diagram to inspect historic data.
   * @param string $user_code The users code (E.g.: 'hs', 'bs', 'rs').
   */
  private function statistics(string $user_code) {
    $user_name = mapUserCodeToName($user_code);

    $metric = $this->input->get('metric');
    $location = $this->input->get('location');

    $this->load->view('frame', [
      'view'      => 'statistics',
      'user_code' => $user_code,
      'user_name' => $user_name,
      'metric'    => $metric,
      'location'  => $location,
      'title'     => 'Wetterstation - ' . $user_name,
			'metric'		=> $this->metricLabel($metric)
    ]);
  } // end statistics()

  public function getStatistics() {
    $user_code = $this->input->post('user_code');
    $date_start = new DateTime($this->input->post('date_start'));
    $date_end = new DateTime($this->input->post('date_end'));
    $location = $this->input->post('location');
    $sensors = getSensors($user_code, $location);

    $weather_events = $this->weather_event_model->getFromTo($date_start, $date_end, $sensors, true);

    // format weather data to be displayed in chart
    $data = [];
    foreach ($weather_events as $weather_event) {
			$date_time = new DateTime($weather_event->date);
			$date_time->sub(new DateInterval('P1M')); // subtract one month
			$date_time->add(new DateInterval('PT1H')); // add one hour
			$date_string = $date_time->format('Y-m-d H:i');
			$date = $date_string; //strtotime($date_string);

      $data['temperature'][] = [
        $date,
				(float) $weather_event->temperature
      ];
      $data['humidity'][] = [
        $date,
				(float) $weather_event->humidity
      ];
      $data['pressure'][] = [
        $date,
				(float) $weather_event->pressure
      ];
    }

    return $this->output
      ->set_header('HTTP/1.1 200 OK')
      ->set_content_type('Content-Type: application/json')
      ->set_output(json_encode($data));
  } // end getStatistics()

	private function metricLabel(string $metric) : string {
		switch ($metric) {
			case 'temperature':
				return 'Temperatur';
				break;
			case 'humidity':
				return 'Luftfeuchtigkeit';
				break;
			case 'pressure':
				return 'Luftdruck';
				break;
			default:
				return '';
		}
	} // end metricLabel

} // end class
