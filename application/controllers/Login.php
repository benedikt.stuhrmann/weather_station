<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct() {
    parent::__construct();

		session_start();
		$logged_in = (array_key_exists('login', $_SESSION) ? $_SESSION['login'] : false) === true;
		if ($logged_in) {
			redirect('home');
		}
  } // end __construct()

  public function index() {
    $this->load->view('frame', [
      'view'            => 'login',
      'title'           => 'Wetterstation'
    ]);
  } // end index()

	public function submit_login_form() {
		$password = $this->input->post('password');
		if (md5($password) === PASSWORD) {
			$this->login();
			redirect('home');
		} else {
			redirect('login');
		}
	} // end submit_login_form()

	private function login() {
		session_destroy();
  	session_start();
		$_SESSION['login'] = true;
	} // end login()

} // end class
