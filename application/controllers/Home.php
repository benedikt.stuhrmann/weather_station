<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct() {
    parent::__construct();

		session_start();
		$logged_in = (array_key_exists('login', $_SESSION) ? $_SESSION['login'] : false) === true;
		if (!$logged_in) {
			redirect('login');
		}
  } // end __construct()

  public function index() {
    $this->load->view('frame', [
      'view'            => 'home',
      'title'           => 'Wetterstation'
    ]);
  } // end index()

} // end class
